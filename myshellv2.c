#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX_LEN 512
#define MAXARGS 10
#define ARGLEN 30
#define PROMPT "PUCIT:- "

int execute(char* argist[]);
char** tokenize(char* cmdline);
char* read_cmd(char*, FILE*);
void sh_exit();
void sh_help();
void sh_cd(char**);

int main()
{
	char *cmdline;
	char** arglist;
	char* prompt = PROMPT;
	while((cmdline=read_cmd(prompt,stdin)) != NULL)
	{
		if((arglist = tokenize(cmdline)) != NULL)
		{
 			// Checking internal commands
			if(strcmp(arglist[0],"exit") == 0)
				sh_exit();
			else if(strcmp(arglist[0],"cd") == 0)
				sh_cd(arglist);
			else if(strcmp(arglist[0],"help") == 0)
				sh_help();
			else
				execute(arglist);
			//free memory
			for(int j=0;j< MAXARGS+1;j++)
				free(arglist[j]);
			free(arglist);
			free(cmdline);
		}
	}
	printf("\n");
	return 0;
}


int execute(char* arglist[])
{
	int status;
	int cpid = fork();
	switch(cpid)
	{
		case -1:
			perror("fork failed");
			exit(1);
		case 0:
			execvp(arglist[0],arglist);
			perror("Command not found");
			exit(1);
		default:
			waitpid(cpid,&status,0);
			printf("Child exited with status %d \n",status>>8);	
			return 0;	
	}
}

char** tokenize(char* cmdline)
{
	char** arglist = (char**)malloc(sizeof(char*)*(MAXARGS+1));
	for(int i=0; i<MAXARGS+1; i++)
	{
		arglist[i] = (char*)malloc(sizeof(char)*ARGLEN);
		bzero(arglist[i],ARGLEN);
	}
	char *cp = cmdline;
	char *start;
	int len;
	int argnum = 0;
	while(*cp != '\0')
	{
		while(*cp == ' ' || *cp == '\t')
			cp++;
		start = cp;
		len = 1;
		while(*++cp != '\0' && !(*cp == ' ' || *cp == '\t'))
			len++;
		strncpy(arglist[argnum], start, len);
		arglist[argnum][len] = '\0';
		argnum++;
	}	
	arglist[argnum] = NULL;
	return arglist;
}

char* read_cmd(char* prompt, FILE* fp)
{
	printf("%s",prompt);
	int c;
	int pos = 0;
	char *cmdline = (char*) malloc(sizeof(char)*MAX_LEN);
	while((c=getc(fp)) != EOF)
	{
		if(c=='\n') break;
		cmdline[pos++] = c;
	}
	if(c== EOF && pos == 0)
		return NULL;
	cmdline[pos] = '\0';
	return cmdline;
}

void sh_exit()
{
	exit(0);
}
void sh_help()
{
	printf("Shaheryar's SHELL\n");
	printf("Type program names and arguments, and hit enter.\n");
	printf("The following are built in:\ncd help exit\n");
	printf("Use man command for information on other programs.\n");
}
void sh_cd(char** arglist)
{
	if (arglist[1] == NULL)
	    fprintf(stderr, "sh: expected argument to \"cd\"\n");
	else 
	{
		char *param = arglist[1];
		int i = 2;
		while(arglist[i] != NULL)
		{
			strcat(param, " ");
			strcat(param, arglist[i++]);
		}
		
		if (chdir(param) != 0) 
		      perror("sh");
	}
}
