#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX_LEN 512
#define MAXARGS 10
#define ARGLEN 30
#define PROMPT "PUCIT:- "

int execute(char* argist[]);
char** tokenize(char* cmdline);
char* read_cmd(char*, FILE*);
void sh_exit();
void sh_help();
void sh_cd(char**);
char** tokenizeBySemicolon(char* cmdline);
int background = 0;
char **history;
void getHistory(char* allHist[],int count);

int main()
{
	char *cmdline;
	char** arglist;
	char** mainArglist;
	char* prompt = PROMPT;
	history = (char**)malloc(sizeof(char*)*(11));
	int x;
	for(x=0; x<10;x++)
    		history[x] = (char*)malloc(sizeof(char)*512);
  	x = 0;
	while((cmdline=read_cmd(prompt,stdin)) != NULL)
	{
		mainArglist=tokenizeBySemicolon(cmdline);
		int k = 0;
		while(mainArglist[k] != NULL)
			k++;
		if( (!strcmp(mainArglist[k-1],"&")) || (!strcmp(mainArglist[k-1]," &")) )
			background = 1;
		else
			background = 0;
		
		for(int i=0;mainArglist[i]!=NULL;i++)
		{
			if( (!strcmp(mainArglist[i],"&")) || (!strcmp(mainArglist[i]," &")) )
				continue;
			else if((arglist = tokenize(mainArglist[i])) != NULL)
			{
				if(x>=10)
					x=0;
				strcpy(history[x++],mainArglist[i]);
	 			// Checking internal commands
				if(strcmp(arglist[0],"exit") == 0)
					sh_exit();
				else if(strcmp(arglist[0],"cd") == 0)
					sh_cd(arglist);
				else if(strcmp(arglist[0],"help") == 0)
					sh_help();
				else if(strcmp(arglist[0],"history") == 0)
					getHistory(history,x);
				else if(arglist[0][0] == '!')
				{
					char* nmbr;
					int len = strlen(arglist[0]);
					strncpy(nmbr, arglist[0], len);
					nmbr[len] = '\0';					
					if(strcmp(nmbr,"!0") == 0)
					{
						arglist = tokenize(history[0]);
						execute(arglist);
					}
					else if(strcmp(nmbr,"!1") == 0)
					{
						arglist = tokenize(history[1]);
						execute(arglist);
					}
					else if(strcmp(nmbr,"!2") == 0)
					{
						arglist = tokenize(history[2]);
						execute(arglist);
					}
					else if(strcmp(nmbr,"!3") == 0)
					{
						arglist = tokenize(history[3]);
						execute(arglist);
					}
					else if(strcmp(nmbr,"!4") == 0)
					{
						arglist = tokenize(history[4]);
						execute(arglist);
					}
					if(strcmp(nmbr,"!5") == 0)
					{
						arglist = tokenize(history[5]);
						execute(arglist);
					}
					else if(strcmp(nmbr,"!6") == 0)
					{
						arglist = tokenize(history[6]);
						execute(arglist);
					}
					else if(strcmp(nmbr,"!7") == 0)
					{
						arglist = tokenize(history[7]);
						execute(arglist);
					}
					else if(strcmp(nmbr,"!8") == 0)
					{
						arglist = tokenize(history[8]);
						execute(arglist);
					}
					else if(strcmp(nmbr,"!9") == 0)
					{
						arglist = tokenize(history[9]);
						execute(arglist);
					}
				}
				else
					execute(arglist);
			}
		}
		//free memory
		for(int j=0;j< MAXARGS+1;j++)
			free(arglist[j]);
		free(arglist);
		free(cmdline);
		for(int j=0;mainArglist[j]!=NULL;j++)
			free(mainArglist[j]);
		free(mainArglist);
	}
	printf("\n");
	return 0;
}


int execute(char* arglist[])
{
	int status;
	int cpid = fork();
	switch(cpid)
	{
		case -1:
			perror("fork failed");
			exit(1);
		case 0:
			execvp(arglist[0],arglist);
			perror("Command not found");
			exit(1);
		default:
			if(background == 0)
			{
				waitpid(cpid,&status,0);
				printf("Child exited with status %d \n",status>>8);	
			}	
			return 0;	
	}
}

char** tokenize(char* cmdline)
{
	char** arglist = (char**)malloc(sizeof(char*)*(MAXARGS+1));
	for(int i=0; i<MAXARGS+1; i++)
	{
		arglist[i] = (char*)malloc(sizeof(char)*ARGLEN);
		bzero(arglist[i],ARGLEN);
	}
	char *cp = cmdline;
	char *start;
	int len;
	int argnum = 0;
	while(*cp != '\0')
	{
		while(*cp == ' ' || *cp == '\t')
			cp++;
		start = cp;
		len = 1;
		while(*++cp != '\0' && !(*cp == ' ' || *cp == '\t'))
			len++;
		strncpy(arglist[argnum], start, len);
		arglist[argnum][len] = '\0';
		argnum++;
	}	
	arglist[argnum] = NULL;
	return arglist;
}


char** tokenizeBySemicolon(char* cmdline)
{
	char** arglist = (char**)malloc(sizeof(char*)*(MAXARGS+1));
	for(int i=0; i<MAXARGS+1; i++)
	{
		arglist[i] = (char*)malloc(sizeof(char)*ARGLEN);
		bzero(arglist[i],ARGLEN);
	}
	char *cp = cmdline;
	char *start;
	int len;
	int argnum = 0;
	while(*cp != '\0')
	{
		while(*cp == ';')
			cp++;
		start = cp;
		len = 1;
		while(*++cp != '\0' && !(*cp == ';'))
			len++;
		strncpy(arglist[argnum], start, len);
		arglist[argnum][len] = '\0';
		argnum++;
	}	
	arglist[argnum] = NULL;
	return arglist;
}

void getHistory(char* allHist[],int count)
{
	if(count > 0)
	{
		for(int i = 0; i<count && allHist[i]!=NULL;i++)
			printf("%d  %s\n",i, allHist[i]);
	}
  	else 
		printf("history: no history found\n");
}

char* read_cmd(char* prompt, FILE* fp)
{
	printf("%s",prompt);
	int c;
	int pos = 0;
	char *cmdline = (char*) malloc(sizeof(char)*MAX_LEN);
	while((c=getc(fp)) != EOF)
	{
		if(c=='\n') break;
		cmdline[pos++] = c;
	}
	if(c== EOF && pos == 0)
		return NULL;
	cmdline[pos] = '\0';
	return cmdline;
}

void sh_exit()
{
	exit(0);
}
void sh_help()
{
	printf("Shaheryar's SHELL\n");
	printf("Type program names and arguments, and hit enter.\n");
	printf("The following are built in:\ncd help exit\n");
	printf("Use man command for information on other programs.\n");
}
void sh_cd(char** arglist)
{
	if (arglist[1] == NULL)
	    fprintf(stderr, "sh: expected argument to \"cd\"\n");
	else 
	{
		char *param = arglist[1];
		int i = 2;
		while(arglist[i] != NULL)
		{
			strcat(param, " ");
			strcat(param, arglist[i++]);
		}
		
		if (chdir(param) != 0) 
		      perror("sh");
	}
}
