#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAX_LEN 512
#define MAXARGS 10
#define ARGLEN 30
#define PROMPT "PUCIT:- "

int execute(char* argist[]);
char** tokenize(char* cmdline);
char* read_cmd(char*, FILE*);
void sh_exit();
void sh_help();
void sh_cd(char**);
int FunForIORedirection(char* arglist[]);
int FunForPipes(char* arglist[]);

int main()
{
	char *cmdline;
	char** arglist;
	char* prompt = PROMPT;
	while((cmdline=read_cmd(prompt,stdin)) != NULL)
	{
		if((arglist = tokenize(cmdline)) != NULL)
		{
 			// Checking internal commands
			if(strcmp(arglist[0],"exit") == 0)
				sh_exit();
			else if(strcmp(arglist[0],"cd") == 0)
				sh_cd(arglist);
			else if(strcmp(arglist[0],"help") == 0)
				sh_help();
			else
				execute(arglist);
			//free memory
			for(int j=0;j< MAXARGS+1;j++)
				free(arglist[j]);
			free(arglist);
			free(cmdline);
		}
	}
	printf("\n");
	return 0;
}


int execute(char* arglist[])
{
	int rv = FunForIORedirection(arglist);
	if(rv==0)
		return 0;
	int rv2 = FunForPipes(arglist);
	if(rv2==0)
		return 0;
	int status;
	int cpid = fork();
	switch(cpid)
	{
		case -1:
			perror("fork failed");
			exit(1);
		case 0:
			execvp(arglist[0],arglist);
			perror("Command not found");
			exit(1);
		default:
			waitpid(cpid,&status,0);
			printf("Child exited with status %d \n",status>>8);	
			return 0;	
	}
}

char** tokenize(char* cmdline)
{
	char** arglist = (char**)malloc(sizeof(char*)*(MAXARGS+1));
	for(int i=0; i<MAXARGS+1; i++)
	{
		arglist[i] = (char*)malloc(sizeof(char)*ARGLEN);
		bzero(arglist[i],ARGLEN);
	}
	char *cp = cmdline;
	char *start;
	int len;
	int argnum = 0;
	while(*cp != '\0')
	{
		while(*cp == ' ' || *cp == '\t')
			cp++;
		start = cp;
		len = 1;
		while(*++cp != '\0' && !(*cp == ' ' || *cp == '\t'))
			len++;
		strncpy(arglist[argnum], start, len);
		arglist[argnum][len] = '\0';
		argnum++;
	}	
	arglist[argnum] = NULL;
	return arglist;
}

char* read_cmd(char* prompt, FILE* fp)
{
	printf("%s",prompt);
	int c;
	int pos = 0;
	char *cmdline = (char*) malloc(sizeof(char)*MAX_LEN);
	while((c=getc(fp)) != EOF)
	{
		if(c=='\n') break;
		cmdline[pos++] = c;
	}
	if(c== EOF && pos == 0)
		return NULL;
	cmdline[pos] = '\0';
	return cmdline;
}

void sh_exit()
{
	exit(0);
}
void sh_help()
{
	printf("Shaheryar's SHELL\n");
	printf("Type program names and arguments, and hit enter.\n");
	printf("The following are built in:\ncd help exit\n");
	printf("Use man command for information on other programs.\n");
}
void sh_cd(char** arglist)
{
	if (arglist[1] == NULL)
	    fprintf(stderr, "sh: expected argument to \"cd\"\n");
	else 
	{
		char *param = arglist[1];
		int i = 2;
		while(arglist[i] != NULL)
		{
			strcat(param, " ");
			strcat(param, arglist[i++]);
		}
		
		if (chdir(param) != 0) 
		      perror("sh");
	}
}


int FunForIORedirection(char* arglist[])
{
	int i=0;
	for(i=0;arglist[i]!=NULL;i++)
	{
		if(strcmp(arglist[i],"<")==0 || strcmp(arglist[i],"0<")==0)
		{
			printf("Input Redirection\n");
			int saved_stdin = dup(0);
			int saved_stdout = dup(1);
			close(0);
			int fd= open(arglist[i+1],O_RDONLY);	
			for(i=0;arglist[i]!=NULL;i++)
			{
				if(strcmp(arglist[i],">")==0 || strcmp(arglist[i],"1>")==0)
				{
					close(1);
					int fd= open(arglist[i+1],O_WRONLY,O_CREAT,O_TRUNC);
				}
			}
			if(fork()==0)
				execlp(arglist[0],"cat",NULL);
			else
			{
				wait(NULL);
				dup2(saved_stdin,0);
				dup2(saved_stdout,1);
				close(saved_stdout);
				close(saved_stdin);
				return 0;
			}
			
		}
		else if(strcmp(arglist[i],">")==0 || strcmp(arglist[i],"1>")==0)
		{
			printf("Output Redirection\n");	
			int saved_stdin = dup(0);
			int saved_stdout = dup(1);
			close(1);
			int fd= open(arglist[i+1],O_CREAT | O_WRONLY | O_RDONLY);
			for(i=0;arglist[i]!=NULL;i++)
			{
				if(strcmp(arglist[i],"<")==0 || strcmp(arglist[i],"0<")==0)
				{
					close(0);
					int fd= open(arglist[i+1],O_RDONLY);
				}
			}
			if(fork()==0)
				execlp(arglist[0],"cat",NULL);
			else
			{
				wait(NULL);
				dup2(saved_stdin,0);
				dup2(saved_stdout,1);
				close(saved_stdout);
				close(saved_stdin);
				return 0;
			 }
		}
	}
	return 1;
}
int FunForPipes(char* args[])
{
	// File descriptors
	int filedes[2]; 
	int filedes2[2];
	
	int num_cmds = 0;
	
	char *command[256];
	
	pid_t pid;
	
	int err = -1;
	int end = 0;
	int i = 0,j = 0,k = 0,l = 0;

	while (args[l] != NULL){
		if (strcmp(args[l],"|") == 0){
			num_cmds++;
		}
		l++;
	}
	num_cmds++;
	
	if(num_cmds ==1)
	{
		//printf("No pipes found");
		return 1;
	}

	while (args[j] != NULL && end != 1){
		k = 0;

		while (strcmp(args[j],"|") != 0){ // command found
			command[k] = args[j];
			j++;	
			if (args[j] == NULL){

				end = 1;
				k++;
				break;
			}
			k++;
		}

		command[k] = NULL;
		j++;		

		if (i % 2 != 0){
			pipe(filedes); // for odd i
		}else{
			pipe(filedes2); // for even i
		}
		
		pid=fork();
		
		if(pid==-1){			
			if (i != num_cmds - 1){
				if (i % 2 != 0){
					close(filedes[1]); // for odd i
				}else{
					close(filedes2[1]); // for even i
				} 
			}			
			printf("Child process could not be created\n");
			return 0;
		}
		if(pid==0){
			// If we are in the first command
			if (i == 0){
				dup2(filedes2[1], STDOUT_FILENO);
			}
			else if (i == num_cmds - 1){
				if (num_cmds % 2 != 0){ // for odd number of commands
					dup2(filedes[0],STDIN_FILENO);
				}else{ // for even number of commands
					dup2(filedes2[0],STDIN_FILENO);
				}

			}else{ // for odd i
				if (i % 2 != 0){
					dup2(filedes2[0],STDIN_FILENO); 
					dup2(filedes[1],STDOUT_FILENO);
				}else{ // for even i
					dup2(filedes[0],STDIN_FILENO); 
					dup2(filedes2[1],STDOUT_FILENO);					
				} 
			}
			
			if (execvp(command[0],command)==err){
				kill(getpid(),SIGTERM);
			}		
		}
				
		// CLOSING DESCRIPTORS ON PARENT
		if (i == 0){
			close(filedes2[1]);
		}
		else if (i == num_cmds - 1){
			if (num_cmds % 2 != 0){					
				close(filedes[0]);
			}else{					
				close(filedes2[0]);
			}
		}else{
			if (i % 2 != 0){					
				close(filedes2[0]);
				close(filedes[1]);
			}else{					
				close(filedes[0]);
				close(filedes2[1]);
			}
		}
				
		waitpid(pid,NULL,0);	
		i++;	
	}
	return 0;
}
	

